﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeBar : MonoBehaviour
{
    [SerializeField]
    private float pValue;

    private Renderer r;

    public float PValue
    {
        get
        {
            return pValue;
        }

        set
        {
            pValue = value;
        }
    }

    void Start()
    {
        r = GetComponent<Renderer>();
    }

    void Update()
    {
        HandleBar();
    }

    public void HandleBar()
    {        
        r.material.SetFloat("_Cutoff", Mathf.InverseLerp(1f, 0.001f, Mathf.Clamp(PValue, 0f, .99f)));
    }
}