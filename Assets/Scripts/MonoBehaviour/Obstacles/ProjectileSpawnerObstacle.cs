﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawnerObstacle : MonoBehaviour
{
    public GameObject arrow;
    public Transform parent;
    public Transform projectileSpawnPoint;
    public float rateOfFire;
    public float speed;

    private GameObject[] arrows;

    void OnEnable()
    {
        arrows = new GameObject[1];

        for (int i = 0; i < arrows.Length; i++)
        {
            arrows[i] = Instantiate(arrow, projectileSpawnPoint.position + Vector3.left, Quaternion.Euler(new Vector3(0, 0, 90)));
            arrows[i].SetActive(false);
            arrows[i].transform.SetParent(parent);
        }
    }

    void Start()
    {

    }

    void FixedUpdate()
    {
        //StartCoroutine(ManageArrows());
        ShootArrows();
    }

    void ShootArrows()
    {
        for (int i = 0; i < arrows.Length; i++)
        {
            arrows[i].SetActive(true);
            arrows[i].transform.Translate(Vector2.up * speed * Time.deltaTime);
        }
    }

    //void ResetArrows()
    //{
    //    for (int i = 0; i < arrows.Length; i++)
    //    {
    //        arrows[i].SetActive(false);
    //        arrows[i].transform.position = new Vector3(projectileSpawnPoint.position.x -1, projectileSpawnPoint.position.y, arrows[i].transform.position.z);
    //    }
    //}

    //IEnumerator ManageArrows()
    //{
    //    ShootArrows();
    //    yield return new WaitForSeconds(rateOfFire);
    //    ResetArrows();
    //}
}
