﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDetector : MonoBehaviour
{
    public GameObject[] alertSymbol;

    void Start()
    {

    }

    void Update()
    {

    }

    public void OnCollisionStay(Collision col)
    {
        if (col.gameObject.tag == "Obstacle")
        {
            foreach (var item in alertSymbol)
            {
                if (!item.activeSelf)
                {
                    item.SetActive(true);
                    item.transform.position = new Vector3(item.transform.position.x, col.gameObject.transform.position.y, item.transform.position.z);
                    break;
                }
            }
        }
    }

    public void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Obstacle")
        {
            foreach (var item in alertSymbol)
            {
                if (!item.activeSelf)
                {
                    item.SetActive(true);
                    item.transform.position = new Vector3(item.transform.position.x, col.gameObject.transform.position.y, item.transform.position.z);
                    break;
                }
            }
        }
    }

    public void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag == "Obstacle")
        {
            foreach (var item in alertSymbol)
            {
                if (item.activeSelf)
                {
                    item.SetActive(false);
                }
            }
        }
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Obstacle")
        {
            foreach (var item in alertSymbol)
            {
                if (item.activeSelf)
                {
                    item.SetActive(false);
                }
            }
        }
    }
}
