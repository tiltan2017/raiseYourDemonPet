﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DestroySelf : MonoBehaviour
{
    private TouchDetection td;
    private RaycastHit hit;

    void Start ()
    {
        td = FindObjectOfType<TouchDetection>();
	}
	
	void Update ()
    {
        if (Equals(SceneManager.GetSceneByName("CurrentLevel"), SceneManager.GetActiveScene()))
            SelfDestruct();
	}

    public void SelfDestruct()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 100f))
        {
            if (hit.collider.gameObject == gameObject)
                Destroy(gameObject);
        }
    }
}
