﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarManager : MonoBehaviour
{
    private const float MINIMUM_HEALTH = 210;
    private const float MAXIMUM_HEALTH = 512;

    public RectTransform barFill;
    public float dropRate;
    public float fillPercentage;

    void Start()
    {

    }

    void FixedUpdate()
    {
        if (barFill.sizeDelta.x == MINIMUM_HEALTH)
            PlayerManager.KillPlayer();
    }

    public void DecreaseHealthValue(float damage)
    {
        barFill.sizeDelta = new Vector2(Mathf.Clamp(barFill.sizeDelta.x - damage, 210, 512), barFill.sizeDelta.y);
    }

    public float GethungerValueByPercentage()
    {
        return (barFill.sizeDelta.x - 210) / (512 - 210);
    }
}
