﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using System.Runtime.Serialization;

[RequireComponent(typeof(BoxCollider2D))]
public class PetManager : MonoBehaviour, IOnTouchEvent
{
    [SerializeField]
    private List<Transform> listOfPoints;
    [SerializeField]
    private Sprite sprite;

    [SerializeField]
    private float speed;
    private float originalSpeed;
    [SerializeField]
    private float distanceToPlayer;
    [SerializeField]
    private float killingDistance;

    [SerializeField]
    private bool isAttacking;
    [SerializeField]
    private bool isGrumble;
    private bool hasGrowled;

    private PlayerManager playerManager;
    private HealthBarManager healthBarManager;
    private AudioSource audioSource;
    private Animator animator;

    public HungerBarManager hungerBar;
    public List<AudioClip> audioClips;
    public float starvationPointByPercentage;
    public float damageToPlayerByPercentage;

    public List<Transform> ListOfPoints
    {
        get
        {
            return listOfPoints;
        }

        set
        {
            listOfPoints = value;
        }
    }

    public Sprite Sprite
    {
        get
        {
            return sprite;
        }

        set
        {
            sprite = value;
        }
    }

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }

    public float DistanceToPlayer
    {
        get
        {
            return distanceToPlayer;
        }
    }

    public bool IsAttacking
    {
        get
        {
            return isAttacking;
        }
    }

    public bool IsGrumble
    {
        get
        {
            return isGrumble;
        }

        set
        {
            isGrumble = value;
        }
    }

    public Manual Manual
    {
        get
        {
            throw new NotImplementedException();
        }
    }
    
    void Awake()
    {
        Time.timeScale = 0;
    }

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        playerManager = FindObjectOfType<PlayerManager>();
        healthBarManager = FindObjectOfType<HealthBarManager>();
        PetData p = new PetData(transform.position, "kuku", 1f, 1f, 1f, false, false, false);
        SavePetData(p, Application.persistentDataPath + "/PetData.json");

        originalSpeed = speed;
    }

    void FixedUpdate()
    {
        PathFinder();
        AttackPlayer();
        OnTouch();
        StartCoroutine(Growl());
    }

    void PathFinder()
    {
        if (!isAttacking)
        {
            if (listOfPoints.Count > 0)
            {
                var closestPoint = listOfPoints[0];

                foreach (var item in listOfPoints)
                {
                    if ((Vector3.Distance(transform.position, item.position)
                        < Vector3.Distance(transform.position, closestPoint.position))
                        && item.position.x > transform.position.x)
                    {
                        closestPoint = item;
                    }
                }

                transform.position = Vector3.MoveTowards(transform.position, closestPoint.position, speed * Time.deltaTime);

                if (Vector3.Distance(transform.position, closestPoint.position) <= 2f)
                {
                    listOfPoints.Remove(closestPoint);
                }
            }           
        }
    }

    void AttackPlayer()
    {
        if (Vector3.Distance(transform.position, playerManager.transform.position) <= distanceToPlayer 
            || transform.position.x > playerManager.transform.position.x)
        {
            isAttacking = true;
            speed = originalSpeed;
            //Debug.Log("Attacking");

            //FindObjectOfType<Camera>().GetComponent<CamShake>().CamShakeProcedure(.1f, 2f);
            transform.position = Vector3.MoveTowards(transform.position, playerManager.transform.position, speed * 1.1f * Time.deltaTime);
        }
        else if (Vector3.Distance(transform.position, playerManager.transform.position) > distanceToPlayer)
        {
            isAttacking = false;

            if (Vector3.Distance(transform.position, playerManager.transform.position) > distanceToPlayer * 2)
                speed = speed * 1.01f;
        }
    }

    IEnumerator Growl()
    {
        if (!hasGrowled)
        {
            if (hungerBar.GethungerValueByPercentage() <= starvationPointByPercentage)
            {
                isGrumble = true;
                if (audioSource.clip != audioClips[1])
                    audioSource.clip = audioClips[1];

                audioSource.loop = false;
                audioSource.volume = 1;

                if (!audioSource.isPlaying)
                    audioSource.Play();

                animator.SetBool("isGrowling", true);
                animator.SetBool("isMad", true);

                yield return new WaitForSeconds(audioSource.clip.length);

                isGrumble = false;
                animator.SetBool("isGrowling", false);
                hasGrowled = true;
            }
        }

        if (hungerBar.GethungerValueByPercentage() > starvationPointByPercentage)
        {
            hasGrowled = false;
            animator.SetBool("isMad", false);
        }
    }

    public void OnTouch()
    {
        if (Vector3.Distance(transform.position, playerManager.transform.position) <= killingDistance && !isGrumble)
        {
            if (!playerManager.GetComponentInChildren<ParticleSystem>().isEmitting)
                playerManager.GetComponentInChildren<ParticleSystem>().Play();

            if (audioSource.clip != audioClips[0])
                audioSource.clip = audioClips[0];
            audioSource.loop = true;
            audioSource.volume = .2f;
            if (!audioSource.isPlaying)
                audioSource.Play();
            animator.SetBool("isAttacking", true);
            StartCoroutine(CamShake.CamShakeProcedure(.1f, .5f));
            healthBarManager.DecreaseHealthValue(damageToPlayerByPercentage);
        }
        else if (Vector3.Distance(transform.position, playerManager.transform.position) > killingDistance && !isGrumble)
        {
            playerManager.GetComponentInChildren<ParticleSystem>().Stop();
            audioSource.Stop();
            animator.SetBool("isAttacking", false);
            StopCoroutine(CamShake.CamShakeProcedure(.1f, .5f));
        }
    }

    public static void SavePetData(PetData data, string path)
    {
        JsonSave.Save(data, path);
    }
}

[DataContract]
public struct PetData
{
    [DataMember]
    public Vector3 pos;
    [DataMember]
    public string name;
    [DataMember]
    public float health;
    [DataMember]
    public float sanity;
    [DataMember]
    public float hunger;
    [DataMember]
    public bool isEnraged;
    [DataMember]
    public bool isHungry;
    [DataMember]
    public bool isHealthy;

    public PetData(Vector3 pos, string name, float health, float sanity, float hunger, bool isEnraged, bool isHungry, bool isHealthy)
    {
        this.pos = pos;
        this.name = name;
        this.health = health;
        this.sanity = sanity;
        this.hunger = hunger;
        this.isEnraged = isEnraged;
        this.isHungry = isHungry;
        this.isHealthy = isHealthy;
    }
}
