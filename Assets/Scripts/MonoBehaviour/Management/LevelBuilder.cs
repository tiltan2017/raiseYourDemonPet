﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class LevelBuilder : MonoBehaviour
{
    private List<TileData> listOfTiles = new List<TileData>();

    public List<GameObject> brushes;
    public Transform parent;

    void OnEnable()
    {
        DrawBoard();
    }

    void OnDisable()
    {

    }

    void Start()
    {

    }

    void Update()
    {
        //Selection.gameObjects[0]
    }

    public void DrawBoard()
    {
        JsonSave.Load(Application.persistentDataPath + "/LevelEdit_Demo.json", listOfTiles);
        foreach (var item in listOfTiles)
        {
            var temp = brushes.Find(x => x.gameObject.name.Equals(item.name));
            var tempGo = Instantiate(temp, item.pos, Quaternion.identity) as GameObject;
            tempGo.transform.SetParent(parent);
        }
    }
}
