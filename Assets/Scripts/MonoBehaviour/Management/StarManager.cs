﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

public class StarManager : MonoBehaviour
{
    public AudioClip pickupSound;

    private int id;

    private LevelManager lm;
    private StarData sm;

    public int Id
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }

    void Start()
    {
        lm = FindObjectOfType<LevelManager>();
        sm = new StarData(Random.Range(0, 1000), transform.position);
        id = sm.id;
        //print("Star " + sm.id + " " + sm.position);
    }

    void Update()
    {

    }

    public void PlaySound()
    {
        AudioSource.PlayClipAtPoint(pickupSound, transform.position);
    }
}

[DataContract]
public class StarData
{
    [DataMember]
    public int id;
    public Vector3 position;

    public StarData(int id, Vector3 position)
    {
        this.id = id;
        this.position = position;
    }
}
