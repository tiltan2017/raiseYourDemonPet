﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    LevelManager lm;

	void Start ()
    {
        lm = FindObjectOfType<LevelManager>();

        UpdateStars();
	}
	
	void Update ()
    {
        
	}

    public void UpdateStars()
    {
        GetComponent<Text>().text = lm.StarCount.ToString();
    }
}
