﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HungerBarManager : MonoBehaviour
{
    public RectTransform barFill;
    public float dropRate;
    public float fillPercentage;

	void Start ()
    {
		
	}
	
	void FixedUpdate ()
    {
        DecreaseWidthValue();
    }

    void DecreaseWidthValue()
    {
        barFill.sizeDelta = new Vector2(Mathf.Clamp(barFill.sizeDelta.x - dropRate, 210, 512), barFill.sizeDelta.y);
    }

    public float GethungerValueByPercentage()
    {
        return (barFill.sizeDelta.x - 210) / (512 - 210);
    }
}
