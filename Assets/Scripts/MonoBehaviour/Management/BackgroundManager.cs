﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackgroundManager : MonoBehaviour
{
    public Transform tiles;

    void Start ()
    {
		
	}
	
	void FixedUpdate ()
    {
        if (SceneManager.GetActiveScene().name == "CurrentLevel")
        {
            if (Camera.main.GetComponent<CamManager>().isCameraMoving)
            {
                transform.GetChild(1).Translate(new Vector3(-Camera.main.GetComponent<CamManager>().speed / 500, 0, 0));
                transform.GetChild(2).Translate(new Vector3(-Camera.main.GetComponent<CamManager>().speed / 250, 0, 0));
                transform.GetChild(3).Translate(new Vector3(-Camera.main.GetComponent<CamManager>().speed / 150, 0, 0));
                tiles.Translate(new Vector3(-Camera.main.GetComponent<CamManager>().speed / 100, 0, 0));
            }
        }
	}
}
