﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using System.Linq;

public class LevelEditor : MonoBehaviour
{
    public List<GameObject> brushes;
    public GameObject selectedBrush;

    private TouchDetection td;
    private List<TileData> listOfTiles;
    private List<GameObject> tempList = new List<GameObject>();
    private List<GameObject> notSavedGo = new List<GameObject>();

    public bool isLevelEditMode;

    public Camera cam;

    void Start ()
    {
        listOfTiles = new List<TileData>();

        td = FindObjectOfType<TouchDetection>();

        LoadTiles();
    }
	
	void Update ()
    {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
        if (isLevelEditMode)
            if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                DrawTiles();
#endif

#if UNITY_ANDROID
        foreach (Touch touch in Input.touches)
        {
            int id = touch.fingerId;
            if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(id))
            {
                DrawTiles();
            }
        }
#endif
    }

    private void DrawTiles()
    {
        var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if (Physics.Raycast(ray, out hit, 100f))
                {
                    if (hit.collider.tag == "Wall" || hit.collider.tag == "Obstacle" || hit.collider.tag == "Star" || hit.collider.tag == "Food" || hit.collider.tag == "Goal")
                    {
                        var temp = listOfTiles.Find(x => x.pos.Equals(hit.collider.gameObject.transform.position));
                        listOfTiles.Remove(temp);
                        var temp2 = notSavedGo.Find(x => x.gameObject.transform.position.Equals(hit.collider.gameObject.transform.position));
                        notSavedGo.Remove(temp2);
                        Destroy(hit.collider.gameObject);
                    }
                }
                else
                {
                    GameObject tempGO = Instantiate(selectedBrush, new Vector3(Mathf.Round(pos.x), Mathf.Round(pos.y), 0), Quaternion.identity) as GameObject;
                    listOfTiles.Add(new TileData(tempGO.transform.position, selectedBrush.name));
                    notSavedGo.Add(tempGO);
                    Debug.Log(selectedBrush.name);
                }
            }
        }
#endif

#if UNITY_STANDALONE_WIN || UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, 100f))
            {
                if (hit.collider.tag == "Wall" || hit.collider.tag == "Obstacle" || hit.collider.tag == "Star" || hit.collider.tag == "Food" || hit.collider.tag == "Goal")
                {
                    var temp = listOfTiles.Find(x => x.pos.Equals(hit.collider.gameObject.transform.position));
                    listOfTiles.Remove(temp);
                    var temp2 = notSavedGo.Find(x => x.gameObject.transform.position.Equals(hit.collider.gameObject.transform.position));
                    notSavedGo.Remove(temp2);
                    Destroy(hit.collider.gameObject);
                }
            }
            else
            {
                GameObject tempGO = Instantiate(selectedBrush, new Vector3(Mathf.Round(pos.x), Mathf.Round(pos.y), 0), Quaternion.identity) as GameObject;
                listOfTiles.Add(new TileData(tempGO.transform.position, selectedBrush.name));
                notSavedGo.Add(tempGO);
                Debug.Log(selectedBrush.name);
            }
        }
#endif
    }

    public void SaveTiles()
    {
        JsonSave.Save(listOfTiles, Application.persistentDataPath + "/LevelEdit_Demo.json");
        Debug.Log("Saved");
    }

    public void LoadTiles()
    {
        DestroyUnsavedTiles();

        JsonSave.Load(Application.persistentDataPath + "/LevelEdit_Demo.json", listOfTiles);
        foreach (var item in listOfTiles)
        {
            var temp = brushes.Find(x => x.gameObject.name.Equals(item.name));
            var tempGo = Instantiate(temp, item.pos, Quaternion.identity) as GameObject;
            tempList.Add(tempGo);
        }
        Debug.Log("Loaded");
    }

    private void DestroyUnsavedTiles()
    {
        if (tempList.Count > 0)
        {
            foreach (var item in tempList)
            {
                Destroy(item);
            }
        }

        if (notSavedGo.Count > 0)
        {
            foreach (var item in notSavedGo)
            {
                Destroy(item);
            }
        }

        tempList.Clear();
        notSavedGo.Clear();
    }

    public void SelectBrush(int i)
    {
        selectedBrush = brushes[i];
    }

    public void ToggleResetGuiWindow(GameObject g)
    {
        g.SetActive(!g.activeSelf);
    }

    public void ResetLevel(GameObject g)
    {
        JsonSave.Unload(Application.persistentDataPath + "/LevelEdit_Demo.json", listOfTiles);

        LoadTiles();
        g.SetActive(!g.activeSelf);
    }

    public void MoveCamera(float speed)
    {
        cam.transform.position += new Vector3(speed, 0);
    }
}

[Serializable] [DataContract]
public class TileData
{
    [DataMember]
    public Vector3 pos;
    [DataMember]
    public string name;

    public TileData(Vector3 pos, string name)
    {
        this.pos = pos;
        this.name = name;
    }
}
