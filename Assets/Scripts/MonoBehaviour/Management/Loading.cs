﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loading : MonoBehaviour
{
    LevelManager lm;

	void Start ()
    {
        lm = FindObjectOfType<LevelManager>();
	}
	
	void Update ()
    {
        StartCoroutine(Load());
	}

    IEnumerator Load()
    {
        yield return new WaitForSeconds(5f);
        lm.LoadGameScene("CurrentLevel");
    }
}
