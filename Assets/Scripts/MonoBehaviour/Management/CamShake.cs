﻿using UnityEngine;
using System.Collections;

public class CamShake : MonoBehaviour
{
    static float shakeAmount;
    static float shakeTimer;

    static Vector3 initCamPos;

	void Start ()
    {
        initCamPos = transform.position;
	}
	
	void FixedUpdate ()
    {
        if (shakeTimer > 0)
        {
            initCamPos = new Vector3(transform.position.x, 4.5f, -10f);
            Vector2 shakePos = Random.insideUnitCircle * shakeAmount;
            transform.position = new Vector3(transform.position.x + shakePos.x, transform.position.y + shakePos.y, transform.position.z);
            shakeTimer -= Time.deltaTime;
        }
        //else if (shakeTimer <= 0)
        //    transform.position = initCamPos;
	}

    static void ShakeCamera(float shakePwr, float shakeDur)
    {
        shakeAmount = shakePwr;
        shakeTimer = shakeDur;
    }

    //Use StartCoroutine to execute
    public static IEnumerator CamShakeProcedure(float shakePwr, float shakeDur)
    {
        ShakeCamera(shakePwr, shakeDur);
        yield return new WaitForSeconds(shakeTimer);
        Vector3 currentPos = Camera.main.transform.position;
        Camera.main.transform.position = Vector3.Lerp(currentPos, initCamPos, 100f);
    }
}
