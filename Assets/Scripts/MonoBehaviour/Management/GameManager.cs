﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

public class GameManager : MonoBehaviour
{
    //might change that to classes instead of dictionary
    [SerializeField]
    private Dictionary<ParticleSystem, List<bool>> playerCustoms; //bool for unlocked and bool for equipped
    [SerializeField]
    private Dictionary<Sprite, List<bool>> petCustoms;

    private static bool isAudio = true;

    public static GameSettingsData gameSettingsData = new GameSettingsData(isAudio);

    public Dictionary<ParticleSystem, List<bool>> PlayerCustoms
    {
        get
        {
            return playerCustoms;
        }

        set
        {
            playerCustoms = value;
        }
    }

    public Dictionary<Sprite, List<bool>> PetCustoms
    {
        get
        {
            return petCustoms;
        }

        set
        {
            petCustoms = value;
        }
    }

    public static bool IsAudio
    {
        get
        {
            return isAudio;
        }

        set
        {
            isAudio = value;
        }
    }

    public GameManager(Dictionary<ParticleSystem, List<bool>> playerCustoms, Dictionary<Sprite, List<bool>> petCustoms, bool isTimeAttack)
    {
        this.PlayerCustoms = playerCustoms;
        this.PetCustoms = petCustoms;
    }

    public void OnEnable()
    {
        isAudio = gameSettingsData.isAudio;

        UpdateAudioListener();
    }

    void Awake()
    {

    }

    void Start()
    {
        LoadGameSettings(gameSettingsData);
    }

    void Update()
    {
        UpdateAudioListener();
    }

    public static void UpdateAudioListener()
    {
        if (FindObjectOfType<Camera>().GetComponent<AudioListener>() != null)
            FindObjectOfType<Camera>().GetComponent<AudioListener>().enabled = isAudio;

        gameSettingsData.isAudio = isAudio;

        SaveGameSettings(gameSettingsData);
    }

    public void LoadLevelSelectionScreen()
    {

    }

    //loads a level first time by getting it's ID
    public void LoadLevel(LevelStatus levelStatus)
    {

    }

    public void ExitGame()
    {

    }

    public void EnterShop()
    {

    }

    public void LoadNextLevel()
    {

    }

    public void ReplayLevel()
    {

    }

    public static void SaveGameSettings(GameSettingsData data)
    {
        JsonSave.Save(data, Application.persistentDataPath + "/GameSettings.json");
    }

    public static void LoadGameSettings(GameSettingsData data)
    {
        JsonSave.Load(Application.persistentDataPath + "/GameSettings.json", ref data);
    }
}

[DataContract]
public class GameSettingsData
{
    [DataMember]
    public bool isAudio;

    public GameSettingsData(){}

    public GameSettingsData(bool isAudio)
    {
        this.isAudio = isAudio;
    }
}
