﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TagManager : MonoBehaviour
{
    private TouchDetection td;

    public TouchDetection Td
    {
        get
        {
            return td;
        }

        set
        {
            td = value;
        }
    }
    
    void Start ()
    {
        td = FindObjectOfType<TouchDetection>();
	}
	
	void Update ()
    {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
        if (td.MouseInteractionDetection())
            HandleInteractions();
        if (Input.GetMouseButtonDown(0))
        {
            switch (ReturnTag())
            {
                case "Sound":
                    td.ReturnHit().collider.gameObject.GetComponent<SpriteRenderer>().enabled
                        = !td.ReturnHit().collider.gameObject.GetComponent<SpriteRenderer>().enabled;
                    GameManager.IsAudio = !GameManager.IsAudio;
                    GameManager.UpdateAudioListener();
                    break;
                default:
                    break;
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (FindObjectOfType<LevelManager>() != null)
                FindObjectOfType<LevelManager>().HasPlayerStarted = false;
            if (FindObjectOfType<PlayerManager>() != null)
                //if (FindObjectOfType<PlayerManager>().transform.GetChild(0).gameObject.activeSelf)
                {
                    if (SceneManager.GetActiveScene().name == "CurrentLevel")
                    {
                        StopAllCoroutines();
                        PlayerManager.KillPlayer();
                    }
                }
        }
#endif

#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            if (td.TouchInteractionDetection())
                HandleInteractions();
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                switch (ReturnTag())
                {
                    case "Sound":
                        td.ReturnHit().collider.gameObject.GetComponent<SpriteRenderer>().enabled
                            = !td.ReturnHit().collider.gameObject.GetComponent<SpriteRenderer>().enabled;
                        GameManager.IsAudio = !GameManager.IsAudio;
                        GameManager.UpdateAudioListener();
                        break;
                    default:
                        break;
                }
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                if (FindObjectOfType<LevelManager>() != null)
                    FindObjectOfType<LevelManager>().HasPlayerStarted = false;
                if (FindObjectOfType<PlayerManager>() != null)
                    //if (FindObjectOfType<PlayerManager>().transform.GetChild(0).gameObject.activeSelf)
                    {
                        if (SceneManager.GetActiveScene().name == "CurrentLevel")
                        {
                            StopAllCoroutines();
                            PlayerManager.KillPlayer();
                        }
                    }
            }
        }
#endif
    }

    public string ReturnTag()
    {
        if (td != null)
        {
            RaycastHit hit = td.ReturnHit();

            if (hit.collider != null)
                return hit.collider.gameObject.tag;
        }
        return null;
    }

    public string ReturnTag2D()
    {
        if (td != null)
        {
            RaycastHit2D hit2D = td.ReturnHit2D();

            if (hit2D.collider != null)
                return hit2D.collider.gameObject.tag;
        }
        return null;
    }

    public void HandleInteractions()
    {
        switch (ReturnTag())
        {
            case "Obstacle":
                if (Equals(SceneManager.GetSceneByName("CurrentLevel"), SceneManager.GetActiveScene()))
                    PlayerManager.KillPlayer();
                break;
            //case "Pet":
            //    if (Equals(SceneManager.GetSceneByName("CurrentLevel"), SceneManager.GetActiveScene()))
            //        PlayerManager.KillPlayer();
            //    break;
            case "Wall":
                if (Equals(SceneManager.GetSceneByName("CurrentLevel"), SceneManager.GetActiveScene()))
                {
                    var tempAS = td.ReturnCollider().GetComponentInParent<AudioSource>();
                    tempAS.Play();
                    Destroy(td.ReturnCollider().gameObject);

                    FindObjectOfType<HealthBarManager>().DecreaseHealthValue(5f);
                }
                break;
            case "Goal":
                if (Equals(SceneManager.GetSceneByName("CurrentLevel"), SceneManager.GetActiveScene()))
                    SceneManager.LoadScene("WinningScreen");
                break;
            case "Portal":
                SceneManager.LoadScene("LevelSelection");
                break;
            case "Bed":
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
                Application.Quit();
#endif
#if UNITY_ANDROID
                SceneManager.LoadScene("MainMenu");
#endif
                break;
            case "Start":
                Time.timeScale = 1;
                if (FindObjectOfType<PlayerManager>().transform.GetChild(0) != null)
                {
                    if (Equals(SceneManager.GetSceneByName("CurrentLevel"), SceneManager.GetActiveScene()))
                        FindObjectOfType<PlayerManager>().transform.position = new Vector3(td.CurrentPos.x, td.CurrentPos.y, 1f);
                }
                if (FindObjectOfType<LevelManager>() != null)
                    FindObjectOfType<LevelManager>().HasPlayerStarted = true;

                FindObjectOfType<LevelManager>().GetComponent<AudioSource>().Play();
                break;
            case "Player":
                FindObjectOfType<LevelManager>().HasPlayerStarted = true;
                break;
            case "Settings":
                SceneManager.LoadScene("Settings");
                break;
            case "Star":
                if (!Equals(SceneManager.GetSceneByName("LevelEditor"), SceneManager.GetActiveScene()))
                {
                    var go = td.ReturnHit(Input.mousePosition).gameObject.GetComponent<StarManager>();

                    if (go != null)
                    {
                        //print(go.Id);
                        go.PlaySound();
                        //if (FindObjectOfType<PlayerManager>().starParticles.isStopped)
                        //    FindObjectOfType<PlayerManager>().starParticles.Play();
                        //FindObjectOfType<PlayerManager>().starParticles.transform.position = go.transform.position;
                        go.gameObject.SetActive(false);
                        FindObjectOfType<LevelManager>().StarCount++;
                        //print(FindObjectOfType<LevelManager>().StarCount);
                    }
                }
                break;
            case "Food":
                if (Equals(SceneManager.GetSceneByName("CurrentLevel"), SceneManager.GetActiveScene()))
                {
                    var temp = FindObjectOfType<HungerBarManager>();

                    temp.GetComponent<AudioSource>().Play();
                    temp.barFill.sizeDelta = new Vector2(Mathf.Clamp(temp.barFill.sizeDelta.x + (512 - 210) / 100 * temp.fillPercentage, 210, 512), temp.barFill.sizeDelta.y);
                }
                break;
            default:
                break;
        }
    }

    public void HandleInteractions2D()
    {
        switch (ReturnTag2D())
        {
            case "Obstacle":
                if (FindObjectOfType<PlayerManager>().transform.GetChild(0).gameObject.activeSelf)
                    PlayerManager.KillPlayer();
                break;
            default:
                break;
        }
    }
}
